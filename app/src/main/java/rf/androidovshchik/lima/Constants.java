package rf.androidovshchik.lima;

public interface Constants {

    String ACTION_STOP = "stop";
    String ACTION_RESTART = "restart";

    String EXTRA_TEXT = "text";
}
