package rf.androidovshchik.lima.receivers;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import rf.androidovshchik.lima.Constants;
import rf.androidovshchik.lima.service.SpeechService;

public class ServiceReceiver extends BroadcastReceiver {

	@Override
	@SuppressWarnings("ConstantConditions")
	public void onReceive(Context context, Intent intent) {
		Log.d(getClass().getSimpleName(), intent.getAction());
		Intent service = SpeechService.getStartIntent(context);
		switch (intent.getAction()) {
			case Constants.ACTION_STOP:
				if (SpeechService.isRunning(context)) {
					context.stopService(service);
				}
				Intent receiver = new Intent(context, SpeechService.class);
				receiver.setAction(Constants.ACTION_RESTART);
				PendingIntent pendingReceiver = PendingIntent.getBroadcast(context,
					0, receiver, PendingIntent.FLAG_UPDATE_CURRENT);
				((AlarmManager) context.getSystemService(Context.ALARM_SERVICE))
					.cancel(pendingReceiver);
				break;
			case Constants.ACTION_RESTART:
				if (!SpeechService.isRunning(context)) {
					context.startService(service);
				}
				break;
		}
	}
}
