package rf.androidovshchik.lima.receivers;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.webkit.JavascriptInterface;

import rf.androidovshchik.lima.Constants;
import rf.androidovshchik.lima.service.SpeechService;

@SuppressWarnings("unused")
public class JavaScriptReceiver {

	private Context context;

	private SharedPreferences prefs;

	public JavaScriptReceiver(Context context) {
		this.context = context;
		prefs = context.getSharedPreferences(SpeechService.PREFS, Context.MODE_PRIVATE);
	}

	@JavascriptInterface
	public void startService(){
		Log.d(getClass().getSimpleName(), "startService");
		prefs.edit().putBoolean(SpeechService.PREF_RUN_SERVICE, true).apply();
		Intent service = SpeechService.getStartIntent(context);
		if (!SpeechService.isRunning(context)) {
			context.startService(service);
		}
	}

	@JavascriptInterface
	@SuppressWarnings("all")
	public void stopService(){
		Log.d(getClass().getSimpleName(), "stopService");
		new AsyncTask<Void, Void, Void>() {

			@Override
			protected void onPreExecute() {
				Log.d(getClass().getSimpleName(), "onPreExecute");
				Intent service = SpeechService.getStartIntent(context);
				if (SpeechService.isRunning(context)) {
					context.stopService(service);
				}
			}

			@Override
			protected Void doInBackground( Void... voids ) {
				Log.d(getClass().getSimpleName(), "doInBackground");
				Intent receiver = new Intent(context, ServiceReceiver.class);
				receiver.setAction(Constants.ACTION_RESTART);
				PendingIntent pendingReceiver = PendingIntent.getBroadcast(context,
					0, receiver, PendingIntent.FLAG_UPDATE_CURRENT);
				((AlarmManager) context.getSystemService(Context.ALARM_SERVICE))
					.cancel(pendingReceiver);
				prefs.edit().putBoolean(SpeechService.PREF_RUN_SERVICE, false).apply();
				return null;
			}
		}.execute();
	}
}