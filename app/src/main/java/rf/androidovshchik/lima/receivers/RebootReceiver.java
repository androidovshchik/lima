package rf.androidovshchik.lima.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import rf.androidovshchik.lima.service.SpeechService;

public class RebootReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		Intent service = SpeechService.getStartIntent(context);
		SharedPreferences prefs =
			context.getSharedPreferences(SpeechService.PREFS, Context.MODE_PRIVATE);
		if (!SpeechService.isRunning(context)
			&& prefs.getBoolean(SpeechService.PREF_RUN_SERVICE, false)) {
			if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
				context.startForegroundService(service);
			} else {
				context.startService(service);
			}
		}
	}
}