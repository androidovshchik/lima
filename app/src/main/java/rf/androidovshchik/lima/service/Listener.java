package rf.androidovshchik.lima.service;

/**
 * Created by vlad on 20.11.17.
 */

public interface Listener {
	/**
	 * Called when a new piece of text was recognized by the Speech API.
	 *
	 * @param text    The text.
	 * @param isFinal {@code true} when the API finished processing audio.
	 */
	void onSpeechRecognized(String text, boolean isFinal);
}
